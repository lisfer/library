#-*-coding: utf-8 -*-

from flask import render_template, redirect, request, url_for, flash
from sqlalchemy import func 

from library import app
from library.models import Book, Author
from library.forms import BookForm, AuthorForm

db_session = app.db_session



class ElementEdit(object):
    
    def __init__(self, model, templ_info, templ_edit, url_list,
            url_info, form):
        self.model = model
        self.templ_info = templ_info
        self.templ_edit = templ_edit
        # url_for('books_list')
        self.url_list = url_list
        # url_for('book_info', book_name_en=name_en)
        self.url_info = url_info
        self.form = form

    def info(self, name_en):
        element = db_session.query(self.model).filter(
                self.model.name_en == name_en).first()
        if not element:
            return render_template('404.html',
                    msg = u' "{}" not found'.format(name_en))
        if request.method == 'POST':
            return self.edit(element)
        return render_template(self.templ_info, data=element)

    def delete(self, element):
        db_session.delete(element)
        db_session.commit()
        return redirect(url_for(self.url_list))

    def _prepare_edit_form(self, element):
        return self.form(obj=element)

    def _save_update(self, element_form, element=None):
        if element is None:
            element = self.model()
            db_session.add(element)
        element_form.populate_obj(element)
        db_session.commit()
        return element


    def edit(self, element):
        if request.form.get('delete_button'):
            return self.delete(element)
        elif request.form.get('cancel_button'):
            return redirect (url_for(self.url_info, name_en=element.name_en))
        elif request.form.get('edit_button'):
            element_form = self._prepare_edit_form(element)
        else:   # save_button was chosen
            element_form = self.form(request.form)
            if element_form.validate():
                self._save(element_form, element )
                return redirect (url_for(self.url_info, name_en=element.name_en))
        return render_template(self.templ_edit, form=element_form)


    def add(self):
        if request.method == 'POST':
            if request.form.get('cancel_button'):
                return redirect(url_for(self.url_list))
            elem_form = self.form(request.form)
            if elem_form.validate():
                elem = self._save(elem_form)
                return redirect(url_for(self.url_info, name_en=elem.name_en))
        else:
            elem_form = self.form()
        return render_template(self.templ_edit, form=elem_form)



class BookEdit(ElementEdit):

    def __init__(self):
        super(BookEdit, self).__init__(Book, 'book_info.html', 
                'book_edit.html', 'books_list', 'book_info', BookForm)

    def _prepare_edit_form(self, element):
        element.authors2 = [(i.id, i.name) for i in element.authors]
        return self.form(obj=element)

    def _save(self, element_form, element=None):
        if element is None:
            element = self.model()
            db_session.add(element)
        element_form.populate_obj(element)
        element.authors = []
        for item in element_form.authors2.data:
            if 'new' in item and item['new']:
                element.authors.append(Author(item['new']))
            elif 'updated' in item and item['updated']:
                element.authors.append(item['updated'])
        db_session.commit()

        # element.authors = db_session.query(Author).filter(
        #    Author.id.in_(element_form.authors_list.data)).all()
        # db_session.commit()
        return element


class AuthorEdit(ElementEdit):

    def __init__(self):
        super(AuthorEdit, self).__init__(Author, 'author_info.html', 
                'author_edit.html', 'authors_list', 'author_info', AuthorForm)

    def _prepare_edit_form(self, element):
        element.books2 = [(i.id, i.name) for i in element.books]
        return self.form(obj=element)

    def _save(self, element_form, element=None):
        if element is None:
            element = self.model()
            db_session.add(element)
        element_form.populate_obj(element)
        element.books = []
        print element_form.books2.data
        for item in element_form.books2.data:
            if 'new' in item and item['new']:
                element.books.append(Book(item['new']))
            elif 'updated' in item and item['updated']:
                element.books.append(item['updated'])
        #element.books = db_session.query(Book).filter(
        #    Book.id.in_(element_form.books_list.data)).all()
        db_session.commit()
        return element


@app.route('/')
def index():
    return render_template('about.html')


@app.teardown_appcontext
def shutdown_db_session(exception=None):
    db_session.remove()


@app.route('/books/')
def books_list():
    #books = db_session.query(Book, func.group_concat(Author.name,'<!break!>'),
    #            func.group_concat(Author.name_en,'<!break!>')).outerjoin(
    #        Author, Book.authors).group_by(Book).order_by(Book.name).all()
    #books = [[i[0].name_en, 
    #          {'name': i[0].name, 'author': u', '.join(
    #                [u'<a class="element_link" href="/authors/{}">{}</a>'.format(b, a) 
    #                 for a, b in zip(i[1].split('<!break!>'),
    #                                 i[2].split('<!break!>'))])}]
    #            if i[1] is not None else
    #         [i[0].name_en,
    #          {'name':i[0].name, 'author': u'<не указан>'}]
    #         for i in books]
    books = db_session.query(Book).order_by(Book.name).all()
    books = [[i.name_en, {'book': i.name, 'author': i.authors if i.authors else u'<не вказано>'} ]
            for i in books]
    return render_template('book_list.html', data=books, page_name=u'Книгі')


@app.route('/authors/')
def authors_list():
    authors = [[i.name_en, {'author': i.name}]
                for i in db_session.query(Author).order_by(Author.name).all()]
    return render_template('author_list.html', data=authors)

@app.route('/books/<name_en>/', methods=['GET', 'POST'])
def book_info(name_en):
    
    book_obj = BookEdit()
    return book_obj.info(name_en)


@app.route('/books/add/', methods=['GET', 'POST'])
def book_add():
    book_obj = BookEdit()
    return book_obj.add()

@app.route('/authors/<name_en>/', methods=['GET','POST'])
def author_info(name_en):
    author_obj = AuthorEdit()
    return author_obj.info(name_en)

@app.route('/authors/add/', methods=['GET', 'POST'])
def author_add():
    auth_obj = AuthorEdit()
    return auth_obj.add()


def search_word(word):
    result = []
    if len(word) < 3:
        flash(u'Пошук ігнрує слова < 3 букв ({})'.format(word))
    else:
        # searching in Books table
        result.extend([['books', u'{} ({})'.format(i[0].name, i[1]), 
                            i[0].name_en, '/static/img/books.png'] 
                    for i in db_session.query(Book, 
                        func.group_concat(Author.name,', ')).outerjoin(
                            Author, Book.authors).group_by(Book).filter(
                                Book.name.ilike(u'%{}%'.format(word))
                        ).all()])
        # searching in Authors table
        result.extend([['authors', i.name, i.name_en, '/static/img/users.png'] 
                    for i in db_session.query(Author).filter(
                            Author.name.ilike(u'%{}%'.format(word))
                        ).all()])
    return result

def search_range_results(data):
   # count range + removing doubles
    result_range = {}
    for item in data:
        if item[2] not in result_range:
            result_range[item[2]] = {'range': 0, 'row': item}
        else:
            result_range[item[2]]['range'] += 1
    # creating list output
    result = [result_range[i]['row'] + [result_range[i]['range']] 
            for i in result_range]
    return result


@app.route('/search')
def search(word=None):
    word = request.args.get('q')
    result = []
    if word:
        for sub_word in word.split(' '):
            result.extend(search_word(sub_word))
        result = search_range_results(result)
        # soring by range
        result.sort(key=lambda x:x[4])
    else:
        word = ''
    return render_template('search.html', search_word=word, data=result)

@app.route('/delete_package', methods=['GET', 'POST'])
def delete_group_elements():
    main_url = request.values.get('main_url')
    elems = request.values.getlist('package[]')
    element_type = [i for i in main_url.split('/') if i][-1]
    if element_type == 'books':
        books = db_session.query(Book).filter(Book.name_en.in_(elems)).all()
        for i in books: 
            db_session.delete(i)
    elif element_type == 'authors':
        authors = db_session.query(Author).filter(Author.name_en.in_(elems)).all()
        for i in authors: 
            db_session.delete(i)
    db_session.commit()
    return ''

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404
