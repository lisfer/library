from sqlalchemy import Table, Column, Integer, String, ForeignKey, Date, func
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declarative_base
from library import app
from library.translit import transliterate

Base = declarative_base()
db_session = app.db_session

class Book(Base):
    
    def get_name_en(self):
        name_en_base = transliterate(self.current_parameters['name'])
        name_en = name_en_base
        cnt = 1
        while db_session.query(Book).filter(Book.name_en == name_en).count() > 0:
            cnt += 1
            name_en = u'{}-{}'.format(name_en_base, cnt)
        return name_en

    __tablename__ = 'books'
    id = Column(Integer, primary_key=True)
    created_on = Column(Date, default=func.now())
    modified_on = Column(Date, onupdate=func.now())
    name = Column(String(255), index=True)
    name_en = Column(String(255), unique=True, index=True, 
            default=get_name_en,
            onupdate=get_name_en)
    
    def __init__(self, name=None):
        self.name = name

    def __repr__(self):
        # repr() in python2* should never return unicode
        return '<Book: {}>'.format(repr(self.name))


author_book = Table(
        'author_book', Base.metadata,
        Column('id', Integer, primary_key=True),
        Column('author_id', Integer, ForeignKey('authors.id')),
        Column('book_id', Integer, ForeignKey('books.id')))


class Author(Base):

    def get_name_en(self):
        name_en_base = transliterate(self.current_parameters['name'])
        name_en = name_en_base
        cnt = 0
        while db_session.query(Author).filter(Author.name_en == name_en).count() > 0:
            cnt += 1
            name_en = u'{}-{}'.format(name_en_base, cnt)
        return name_en


    __tablename__ = 'authors'
    id = Column(Integer, primary_key=True)
    created_on = Column(Date, default=func.now())
    modified_on = Column(Date, onupdate=func.now())
    name = Column(String(255), index=True)
    name_en = Column(String(255), unique=True, index=True, 
            default=get_name_en,
            onupdate=get_name_en)
    books = relationship('Book', backref=backref('authors', order_by=name), secondary=author_book, order_by=Book.name)
    
    def __init__(self, name=None):
        self.name = name

    def __repr__(self):
        return '<Author: {}>'.format(repr(self.name))
