$(document).ready(function(){

    var SELECT_WIDGET = null;

    function safe_tags(str) {
        return str.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;') ;
    }

    var get_linked_names = function() {
        var linked = $('.select-widget-element span.value');
        var names = []
        for (var i = 0; i < linked.length - 1; i ++) {
            names[names.length] = $(linked[i]).text().trim();
        }
        return names;
    }

    var recalc_select_range_height = function() {
        var range_elem = $('.select-widget-range');
        var height = parseInt($('#footer').offset()['top']);
        height -=  205;
        //if ($('#element_row_container').offset().top < $(window).scrollTop()) {
        //    height -= 50;
        //}
        height += 'px';
        range_elem.css('height', height);
    }

    var hide_lineked_elements = function(){
        var range_elem = $('.select-widget-range');
	linked = get_linked_names();
        range_elem.find('.select-widget-option').each(function(){
            if (linked.indexOf($(this).find('span.value').text().trim()) != -1) {
                $(this).hide();
            }
        });

    }

    var show_select_range = function(element) {

        var range_elem = $('.select-widget-range');
	hide_lineked_elements();
        
        $('#element_row_container').css('float', 'left'); 
        $(element).css('background-color', '#eee')
        $(element).find('.select-element-cell-triangle').show("slow")
        range_elem.css('width', '0px');
        recalc_select_range_height();
        // if ($('#element_row_container').offset().top < $(window).scrollTop()) {
        //    range_elem.css('margin-top', $(window).scrollTop() - 200 + 'px');
        //} else {
        //    range_elem.css('margin-top', '0px');
        //}
        range_elem.show();
        //$('.select-widget-range').animate({'width': '300px'}, 500)
        range_elem.animate({'width': '300px'}, 300);
    }

    var close_select_range = function(open_next) {

        $('.select-element-cell-triangle').hide("slow");
        $('.select-widget-range').animate({'width': '0px'}, 300, function(){
            $('#element_row_container').css('float', ''); 
            $(SELECT_WIDGET).css('background-color', '')
            $(this).hide();
            
            // showing current element from select square
            var id = $(SELECT_WIDGET).find('.select-widget-element').attr('data-id');
             $('.select-widget-option').show();
            
            SELECT_WIDGET = null;
            if (open_next)  open_next.click();
        });
    }


    var click_select_element = function(elem) {
        if (! SELECT_WIDGET) {
            SELECT_WIDGET = elem;
            // hiding current element from select square
            var id = $(elem).find('.select-widget-element').attr('data-id');
            $('.select-widget-range #select-widget-option_' + id).hide();
            show_select_range(elem);                        
        } else {
            var open_next = (SELECT_WIDGET != elem) ? $(elem) : null;
            close_select_range(open_next);
        }
        return false;   // stop html click event
    }

    var delete_row = function (elem) {
        if (!$(elem).is('.delete_button:first')) {
            //for not first row = just delete current row
            $(elem).closest('.element_row').remove();
        } else {
            //otherwise = replace current cell with the next cell
            //and remove next row
            var curr_cell = $(elem).closest('.element_cell');
            var curr_row = curr_cell.parent();
            var next_row = curr_row.next('.element_row');
            var next_cell = next_row.find('.select-element-cell');
            curr_cell.remove();
            curr_row.append(next_cell);
            next_row.remove();
        }
        return false;
    }

    $('.delete_button').click(function(){
        return delete_row(this);
    });

    $('html').click(function(){
        if (SELECT_WIDGET) {
            close_select_range();
        };
    });

    $('.select-widget-element').parent().click(function(){
        return click_select_element(this);
    });

    var create_new_widget_line = function(id, value, curr_line) {
         var name = $('.select-widget-element input.id:last').attr('name').split('-');
         console.log(name, parseInt(name[1]))
         var new_row = '<div class="element_row"><div class="element_cell"></div>' +
                    '<div class="select-element-cell element_cell " style="padding-right:0">' +
                    '<div class="select-widget-element_new select-widget-element">' +
                    '<input name="' + name[0] + '-' +   (1 + parseInt(name[1])) +
                    '" "class="id" type="hidden" value="' + id +'">' +
                    '<span class="value">' + safe_tags(value) + '</span> ' +
                    '<input class="delete_button" type="button" value="delete"></div>' +      
                    '<div class="select-element-cell-triangle"></div></div></div>';
            var row_for_new = $(curr_line).parent()
            var par = $(row_for_new).parent();
            row_for_new.remove();
            par.append(new_row);
            $('.select-widget-element:last').parent().click(function(){
                return click_select_element(this);
            });
            par.append(row_for_new);
            $('.select-widget-element:last').parent().click(function(){
                return click_select_element(this);
            });
            $('.delete_button:last').click(function(){
                return delete_row(this);
            });
    $('#input-widget-element-new').keyup(function(){
         filter_options(this.value);
    });

    $('#input-widget-element-new').blur(function(){
	    if($('.select-widget-option:visible').length === 0) {
                add_with_input(this);
	    } else {
		$(this).val('');
	    }
	    close_select_range();
        });

    recalc_select_range_height();

    } 

    $('.select-widget-option').click(function(){
        var id = $(this).find('.id').text();
        var value = $(this).find('.value').text();
        if ($(SELECT_WIDGET).find('#input-widget-element-new').length > 0) {
                create_new_widget_line(id, value, SELECT_WIDGET)
           } else {
            $(SELECT_WIDGET).find('.id').val($(this).find('.id').text());
            $(SELECT_WIDGET).find('.value').text($(this).find('.value').text());
        }
        close_select_range();
	return false;
    });

    var filter_options = function(value) {

        filter_str = value.toLowerCase();
        $('.select-widget-option').each(function(){
            if ($(this).text().toLowerCase().indexOf(filter_str) == -1) {
                $(this).hide();
            } else {
                $(this).show();
            }
        });
	hide_lineked_elements();
    }

    $('#input-widget-element-new').keyup(function(){
         filter_options(this.value);
    });

    $('#input-widget-element-new').click(function(){

        if (SELECT_WIDGET) {
            if ($(SELECT_WIDGET)[0] == $(this).parent().parent()[0]) {
                filter_options(this.value);
                return false;
            } else {
                /* TODO: close_select_range();
                click_select_element($(this).parent());
                filter_options($(this).value);
                */
            }
        } else {
            // click_select_element($(this).parent());
            filter_options(this.value);
        }
        // return false;
    });
   

    var add_with_input = function (inp_field) {
        if (inp_field.value) {
            var linked_names =  get_linked_names()
            if (linked_names.indexOf(inp_field.value) != -1) {
                inp_field.value = '';
                return false;
            }
            create_new_widget_line(inp_field.value, inp_field.value, $(inp_field).parent().parent())
            inp_field.value = '';
        }
    }
    $('#input-widget-element-new').blur(function(){
	    if($('.select-widget-option:visible').length === 0) {
                add_with_input(this);
	    } else {
		$(this).val('');
	    }
	    close_select_range();
    });
    $('#input-widget-element-new').keypress(function(e){
        if(e.keyCode == 13) {
            add_with_input(this);
            return false;
        }
    })
});
