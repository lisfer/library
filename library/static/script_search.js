$(document).ready(function(){
    var search_word = $('#search-field').val();
    var found = $('.element_cell');
    if (search_word && found.length > 0) {
        search_word = search_word.split(' ');
        found.each(function(){
            var new_val = $(this).html();
            for (var i = 0;i < search_word.length; i++) {
                if (search_word[i]){
                    var re = new RegExp(search_word[i], 'gi');
                    new_val = new_val.replace(re, '<span class="span-found-lighted">$&</span>');
                }
            }
            $(this).html(new_val);

        });
    }
});
