$(document).ready(function(){
    
    LAST_SELECTED_ROW = null;
    
    var set_color = function(elem) {
        $(elem).attr('data-selected', 'true');
        $(elem).attr('data-base-color', $(elem).css('background-color'));
        $(elem).css('background-color','#55E055');
    }

    var unset_color = function(elem) {
        $(elem).removeAttr('data-selected');
        $(elem).css('background-color',$(elem).attr('data-base-color'));
    }

    var change_color = function(elem) {
        if ($(elem).attr('data-selected') == 'true') {
            unset_color(elem)
        } else {
            set_color(elem)
        }
        if ($('.element_row[data-selected="true"]').length > 0) {
            $('#del_btn').show();
        } else {
            $('#del_btn').hide();
        }

    }
    
    var unselect_all = function() {
        selected = $('.element_row[data-selected="true"]');
        for (i=0; i < selected.length; i++) {
            unset_color(selected[i]);
        }
    }

    
    var open_info = function(element) {
        change_color(element);
        location.href += $(element).attr('data-href');   
    }

    
    var select_line = function(e, element) {
        if (!e.ctrlKey) {
            
            unselect_all();
            document.getSelection().removeAllRanges();

            if (e.shiftKey && LAST_SELECTED_ROW) {
                var container = $('.element_row');
                var ind1 = container.index(LAST_SELECTED_ROW);
                var ind2 = container.index(element);
                if (ind1 < ind2) {
                    selected = $(LAST_SELECTED_ROW).nextUntil(element, '.element_row') 
                }
                if (ind1 > ind2) {
                    selected = $(LAST_SELECTED_ROW).prevUntil(element, '.element_row') 
                }
                set_color(LAST_SELECTED_ROW)
                for (i=0; i < selected.length; i++) {
                     set_color(selected[i]);
                }
            } else {    // click without modifier-keys
                LAST_SELECTED_ROW = element;
            }
        }

        change_color(element);   
    }

    var prepare_lines = function() {
        $('.element_row.clickable').dblclick(function(){
            open_info(this);
        });
        
        
        $('.element_row.clickable').mousedown(function(e){
            select_line(e, this);       
        });
    }

    prepare_lines();

    $('#del_btn').click(function() {
        var selected = $('.element_row[data-selected="true"]');
        var sel_urls = []
        for (i=0; i<selected.length; i++) {
            sel_urls[sel_urls.length] = $(selected[i]).attr('data-href');
        }
        $.ajax({
            url: '/delete_package',
            type: 'POST',
            data: {'main_url': location.href,
                'package': sel_urls },
            success: function () {
                selected.remove();
                $('#del_btn').hide();
            }
       });
    });

    
    /* sorting */

    $('.element_title_cell').click(function(){
        var sort_it = function (a, b) {
            var val1 = $($(a).find('.element_cell')[col_num]).text();
            var val2 = $($(b).find('.element_cell')[col_num]).text();
            if (direct == '\u2193') {
                return val1 > val2 ? -1 : (val1 == val2 ? 0 : 1);
            }
            return val1 > val2 ? 1 : (val1 == val2 ? 0 : -1);
                        
        }
        var direct = $(this).find('.sort_direct').html();
        var col_num = $(this).prevAll('.element_title_cell').length;
        var rows = $('.element_row').sort(sort_it)
        $('#element_row_container').html(rows)
        /*
         * '\u2193' = arrow down
         * '\u2191' = arrow down
         */ 
        $('.element_title_cell .sort_direct').html('');
        if (direct == '\u2193') {
            var direct = $(this).find('.sort_direct').html('\u2191');
        } else {
            var direct = $(this).find('.sort_direct').html('\u2193');
        }
        unselect_all();
        LAST_SELECTED_ROW = null;
        prepare_lines();
    });

    /* filter */

    $('#filter_field').keyup(function(){
        filter_str = this.value.toLowerCase();
        $('.element_row').each(function(){
            if ($(this).text().toLowerCase().indexOf(filter_str) == -1) {
                $(this).hide();
            } else {
                $(this).show();
            }
        });
    });
})
