#!-*-coding: utf-8 -*-
from wtforms.form import Form
from wtforms import StringField, Field, SelectMultipleField, validators, FieldList
from library import app
from library.models import Author, Book

db_session = app.db_session

def html_escape(text):
    """Produce entities within text."""
    _table = {"&": "&amp;",
                     ">": "&gt;",
                     "<": "&lt;"}
    return "".join(_table.get(c,c) for c in text)



def select_widget(field, **kwargs):
    name = field.name
    rzlt = ''
    id_, val_ = field._value()
    if id_ == '-1':
        rzlt  =  u"""
            <div class="select-widget-element">
                <input id="input-widget-element-new" name="select-widget-element-new" type="text"/>
            <span class="value" style="display:inline">{1}</span></div>
        """.format(id_, val_, name)
    else:
        rzlt  =  u"""
            <div id="select-widget-element_{0}" 
                data-id="{0}"
                class="select-widget-element">
                <input name="{2}" class="id" type="hidden" value="{0}" id="id_{0}"/>
            <span class="value">{1}</span>
            <input class="delete_button" type="button" id="del-btn_{0}" value="delete"/>
            </div>
        """.format(id_, html_escape(val_), name)
    return rzlt


class SelectRangeField(Field):
    widget = select_widget

    def __init__(self, model, label='', validators=None, **kwargs):
        super(SelectRangeField, self).__init__(label, validators, **kwargs)
        self.model = model

    def _value(self):
        if self.data:
            return self.data # tuple
        else:
            return (-1, '+')
   
    def process_formdata(self, valuelist):
        self.data = {'updated': None, 'new':None}
        for i in valuelist:
            try:
                id_ = int(i)
                self.data['updated'] = db_session.query(self.model).filter(
                    self.model.id == id_).first()
            except ValueError:
                self.data['new'] = i


class BookForm(Form):
    name = StringField(u'Назва',[validators.required()])
    authors2 = FieldList(SelectRangeField(Author,u'Автори',  widget=select_widget))

    def __init__(self, *args, **kwargs):
        super(BookForm, self).__init__(*args, **kwargs)
        self.authors2.choices = [(i.id, i.name) 
                for i in db_session.query(Author).order_by(Author.name).all()]
        self.authors2.append_entry(('-1', '+'))
        
class AuthorForm(Form):
    name = StringField(u'Автор',[validators.required()])
    books2 = FieldList(SelectRangeField(Book,u'Книги',  widget=select_widget))

    def __init__(self, *args, **kwargs):
        super(AuthorForm, self).__init__(*args, **kwargs)
        self.books2.choices = [(i.id, i.name) 
                for i in db_session.query(Book).order_by(Book.name).all()]
        self.books2.append_entry(('-1', '+'))
 
