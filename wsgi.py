#!/usr/bin/python
import os
from library import app as application 
from database import db_connect


def set_virtual_env():
    virtenv = os.environ.get('OPENSHIFT_PYTHON_DIR','') + '/virtenv/'
    virtualenv = os.path.join(virtenv, 'bin/activate_this.py')
    try:
        exec_namespace = dict(__file__=virtualenv)
        with open(virtualenv, 'rb') as exec_file:
             file_contents = exec_file.read()
             compiled_code = compile(file_contents, virtualenv, 'exec')
             exec(compiled_code, exec_namespace)
    except IOError:
        pass


if __name__ == '__main__':
    set_virtual_env()

    application.config.from_object('config.ProductConfig')
    db_connect(application)
    from library import main

    from wsgiref.simple_server import make_server
    httpd = make_server('localhost', 8051, application)
    httpd.serve_forever()
