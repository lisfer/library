#! -*- coding: utf-8 -*-
import os
from library import app
from database import db_connect
import unittest
from tests import *

def init_data():
    from library.models import Book, Author
    app.db_session.add(Book(u'Властелин колец'))
    app.db_session.add(Book(u'Властелин колец'))
    app.db_session.add(Book(u'Властелин колец'))
    auth = Author(u'Николай Невский')
    b1 = Book(u'Аврора')
    b1.authors.append(auth)
    app.db_session.add(b1)
    app.db_session.commit()


if __name__ == "__main__":
    app.config.from_object('config.TestConfig')
    db_connect(app, create=True)
    import library.main
    init_data()
    
    unittest.main(exit=False)
    
    os.close(app.config['DATABASE_FILE_HANDLE'])
    os.unlink(app.config['DATABASE_FILE'])


