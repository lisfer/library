#-*- coding: utf-8 -*-

from database import db_connect
from library import app
import os

FORSED = False  # create database even file exists (-f parameter)
SILENT = False  # create empty database (without init data) (-s parameter)
PRODUCT = False

def set_init_data():
    from library.models import Book, Author
    auth1 = Author(u'Клайв Стэйплз Льюис')
    auth1.books = [Book(u'Хроники Амбера'),
                   Book(u'Лев, Колдунья и Платяной шкаф'),
                   Book(u'Принц Каспиан'),
                   Book(u'Покоритель Зари, или Плавание на край света'),
                   Book(u'Серебряное кресло'),
                   Book(u'Конь и его мальчик'),
                   Book(u'Племянник чародея'),
                   Book(u'Последняя битва')]
    auth_peh = Author(u'Алексей Пехов')
    auth_peh.books = [Book(u'Крадущийся в тени'),
                   Book(u'Джанга с тенями'),
                   Book(u'Вьюга теней')]
    book1 = Book(u'Заклинатели')
    book1.authors = [auth_peh,
            Author(u'Елена Бычкова'),
            Author(u'Наталья Турчанинова')]
    book2 = Book(u'Библия')
    app.db_session.add_all([auth1, book1, book2])
    app.db_session.commit()


def create_file():
    from sys import argv
    FORSED = '-f' in argv[1:] 
    SILENT = '-s' in argv[1:]
    PRODUCT = 'production' in argv[1:]
    if PRODUCT:
        app.config.from_object('config.ProductConfig')
    if os.path.isfile(app.config['DATABASE_FILE']):
        if not FORSED:
            return
        os.remove(app.config['DATABASE_FILE'])
    db_connect(app, True)
    if not SILENT:
        set_init_data()
        

if __name__ == "__main__":
    create_file()
   

