#! -*- coding: utf-8 -*-
from library import app
from database import db_connect

if __name__ == "__main__":
    app.config.from_object('config.DevelopConfig')
    db_connect(app)
    import library.main
    app.run(debug=True)
