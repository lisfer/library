#-*- coding: utf-8

from library import app
from flask.ext.testing import TestCase
import unittest

class LibraryTestCase(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_book_name_en(self):
        from library.models import Book
        b = app.db_session.query(Book).all()
        b = app.db_session.query(Book).filter(Book.id == 2).first()
        assert b.name_en == 'vlastelyn-kolets-2'


class ViewTest(TestCase):

    def create_app(self):
        return app

    def test_search(self):
        from library.models import Book
        resp = self.client.get(u'/search?q=кол')
        found = self.get_context_variable('data')
        assert len(found) == 4
        assert found[3][2] == u'nykolaj-nevskyj'
        assert found[1][2] == u'vlastelyn-kolets-2'

if __name__ == '__main__':
    unittest.main()

