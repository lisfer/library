from setuptools import setup

setup(name='BookShell',
      version='1.0',
      description='OpenShift App',
      author='Markushin Denis',
      author_email='makrushinds@gmail.com',
      url='http://www.python.org/sigs/distutils-sig/',
      install_requires=['Flask==0.10.1',
                        'Jinja2==2.7.3',
                        'MarkupSafe==0.23',
                        'SQLAlchemy==0.9.8',
                        'WTForms==2.0.1',
                        'Werkzeug==0.9.6',
                        'argparse==1.2.1',
                        'itsdangerous==0.24',
                        'wsgiref==0.1.2'],
     )
