from sqlalchemy import create_engine, event
from sqlalchemy.pool import Pool
from sqlalchemy.orm import scoped_session, sessionmaker

db_session = None
engine = None

def db_connect(app, create=False):
    
    def db_init():
        from library import models 
        models.Base.metadata.create_all(bind=engine)
    
    engine = create_engine(app.config['DATABASE_URI'], convert_unicode=True, echo=app.config['DATABASE_ECHO'])
    db_session = scoped_session(sessionmaker(autocommit=False,
                autoflush=False, bind=engine))
    app.db_session = db_session
    from library.models import Base
    Base.query = db_session.query    
    if create:
        db_init()

@event.listens_for(Pool, "connect")
def register_function(dbapi_conn, conn_record):
    dbapi_conn.create_function("lower", 1, lambda x:x.lower())


