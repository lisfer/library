import os

class Config(object):
    BASEDIR = os.path.abspath(os.path.dirname(__file__))
    DATABASE_FILE = os.path.join(BASEDIR, 'database.db')
    DATABASE_URI = 'sqlite:///' + DATABASE_FILE
    DATABASE_ECHO = False
    CSRF_ENABLED = True
    SECRET_KEY = 'asdf@#$afdsf-04hhfs0v9xc8s3' 

class ProductConfig(Config):
    DATABASE_FILE = os.path.join(os.environ.get('OPENSHIFT_DATA_DIR',''), 'database.db')
    DATABASE_URI = 'sqlite:///' + DATABASE_FILE

class DevelopConfig(Config):
    DATABASE_ECHO = True

class TestConfig(Config):
    import tempfile
    DATABASE_FILE_HANDLE, DATABASE_FILE = tempfile.mkstemp()
    DATABASE_URI = 'sqlite:///' + DATABASE_FILE
    TESTING = True

